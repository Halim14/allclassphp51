<?php
include_once "header.php";
?>
<?php
// connection
$db = new PDO('mysql:host=localhost;dbname=bitmphp51;charset=utf8mb4', 'root', '');
//
//build query
$query = "SELECT * FROM `students` ORDER BY id DESC";
//var_dump($query);
//execution
$stmt = $db->query($query);
$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
//var_dump($student);

?>

    <div class="container">
        <div class="row">
            <div id="wrap" class=" col-md-offset-2 col-md-8 ">
                <div class="panel-heading">
                    <h2>Show Student :<span class="pull-right"> <strong>Welcome!</strong></span> </h2>
                </div>
                <div class="panel-body">
                    <div class="table-bordered">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Full Name</th>
                                <th>Seip id</th>
                                <th>Program Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($students as $student){

                            ?>
                            <tr>
                                <td><?=$student['id'];?></td>
                                <td><?=$student['first_name'].' '.$student['last_name'];?></td>
                                <td><?=$student['seip'];?></td>
                                <td><?=$student['Program_name'];?></td>

                                <td>
                                    <a href="show.php?id=<?=$student['id'];?>">Show</a> |
                                    <a href="edit.php?id=<?=$student['id']?>">Edit</a> |
                                    <a href="delete.php?id=<?=$student['id']?>">Delete</a>
                                </td>

                            </tr>
                            <?php
                            }

                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include_once "footer.php";?>