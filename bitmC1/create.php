<?php include_once "header.php";?>
<div class="container">
    <div class="row">
        <div id="wrap" class=" col-md-offset-2 col-md-8 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Add a Student :<span class="pull-right"> <strong>Welcome!</strong></span> </h2>
                </div>
                <div class="panel-body">
                    <div style="max-width: 400px; margin: auto">
                        <form action="store.php" method="post">
                            <div class="form-group">
                                <label for="email">First Name</label>
                                <input type="text" name="first_name" class="form-control" placeholder="Enter your first name">
                                <label for="email">Last Name</label>
                                <input type="text" name="last_name" class="form-control" placeholder="Enter your last name">
                                <label for="email">SEIP id</label>
                                <input type="text" name="seip" class="form-control" placeholder="Enter your seip id">
                                <label for="email">Program Name</label>
                                <input type="text" name="p_name" class="form-control" placeholder="Enter your Program name">
                            </div>
                            <input type="submit" name="submit" class="myButton" value="Login">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "footer.php";?>