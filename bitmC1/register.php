<?php include_once "header.php";?>
    <div class="container">
        <div class="row">
            <div id="wrap" class=" col-md-offset-2 col-md-8 ">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>User list<span class="pull-right"> <strong>Welcome!</strong></span> </h2>
                    </div>
                    <div class="panel-body">
                        <div style="max-width: 300px; margin: auto">
                            <form action="" method="">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" id="name" name="name" class="form-control">
                                    <label for="username">Username</label>
                                    <input type="text" id="username" name="username" class="form-control">
                                    <label for="email">Email</label>
                                    <input type="text" id="email" name="email" class="form-control">
                                    <label for="password">Password</label>
                                    <input type="password" id="password" name="password" class="form-control">
                                </div>
                                <input type="submit" name="register" class="myButton" value="Submit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
<?php include_once "footer.php";?>