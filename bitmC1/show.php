<?php
include_once "header.php";

?>
<?php
//connection
$db = new PDO('mysql:host=localhost;dbname=bitmphp51;charset=utf8mb4', 'root', '');

//build query
//$query = "SELECT * FROM `students` ORDER BY id DESC LIMIT 0,5";
$query = "SELECT * FROM `students` WHERE id =".$_GET['id'];
//execution
$stmt = $db->query($query);
$student = $stmt->fetch(PDO::FETCH_ASSOC);
//var_dump($student);

?>

<div class="container">
    <div class="row">
        <div id="wrap" class=" col-md-offset-2 col-md-8 ">
            <div class="panel-heading">
            <h2>Single Student :<span class="pull-right"> <strong>Welcome!</strong></span> </h2>
        </div>
            <div class="panel-body">
                    <div class="table-bordered">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Full Name</th>
                                <th>Seip id</th>
                                <th>Program Name</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td><?=$student['id'];?></td>
                                <td><?=$student['first_name'].' '.$student['last_name'];?></td>
                                <td><?=$student['seip'];?></td>
                                <td><?=$student['Program_name'];?></td>

<!--                                <td>-->
<!--                                    <a href="show.php?id=--><?//=$student['id']?><!--">Show</a> |-->
<!--                                    <a href="student/edit.php?id=--><?//=$student['id']?><!--">Edit</a> |-->
<!--                                    <a href="student/delete.php?id=--><?//=$student['id']?><!--">Delete</a>-->
<!--                                </td>-->

                            </tr>

                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "footer.php";?>