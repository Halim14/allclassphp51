<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Register</title>
    <!-- Link-->
    <link href="inc/bootstrap.min.css" rel="stylesheet">
    <link href="inc/style.css" rel="stylesheet">
    <script src="inc/jquery.min.js"></script>
    <script src="inc/bootstrap.min.js"></script>
    <script src="inc/style.js"></script>
    <!--End of Link-->
</head>
<body>

<div class="container">

    <!--    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">-->
    <nav class="navbar navbar-default navbar-fixed-top sticky-navbar" role="navigation">
        <div class="container-fluid">
            <div  class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#appzi-navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Login Register</a>
            </div>
            <div class="navbar-collapse collapse" id="appzi-navigation">
                <ul class="nav navbar-nav pull-right">
                    <li><a href="profile.php">Profile</a></li>
                    <li><a href="">Logout</a></li>
                    <li><a href="login.php">Login</a></li>
                    <li><a href="register.php">Register</a></li>
                </ul>
            </div>
    </nav>
</div>