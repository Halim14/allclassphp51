<?php
include 'lib/header.php';
include 'lib/User.php';
$user = new User();
?>
<div class="container">
        <div class="row">
            <div id="wrap" class=" col-md-offset-2 col-md-8 ">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>User list<span class="pull-right"> <strong>Welcome!</strong>Halim</span> </h2>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <th width="20%">Serial no</th>
                            <th width="20%">Name</th>
                            <th width="20%">Username</th>
                            <th width="20%">Email Address</th>
                            <th width="20%">Action</th>
                            <tr>
                                <td>01</td>
                                <td>Halim</td>
                                <td>Halim</td>
                                <td>Halim@gmail.com</td>
                                <td>
                                    <a href="profile.php?id=1">View</a>
                                </td>
                            </tr>
                            <tr>
                                <td>02</td>
                                <td>Khan</td>
                                <td>Khan</td>
                                <td>Khan@gmail.com</td>
                                <td>
                                    <a href="profile.php?id=1">View</a>
                                </td>
                            </tr>
                            <tr>
                                <td>03</td>
                                <td>Sujon</td>
                                <td>Sujon</td>
                                <td>Sujon@gmail.com</td>
                                <td>
                                    <a href="profile.php?id=1">View</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
             </div>
    </div>
</div>
<?php
include 'lib/footer.php';
?>