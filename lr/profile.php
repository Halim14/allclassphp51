<?php
include 'lib/header.php';
?>
    <div class="container">
        <div class="row">
            <div id="wrap" class=" col-md-offset-2 col-md-8 ">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>User Profile<span class="pull-right"><a class="btn btn-primary" href="index.php">Back</a></span> </h2>
                    </div>
                    <div class="panel-body">
                        <div style="max-width: 300px; margin: auto">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="name">Your name</label>
                                    <input type="text" name="name" class="form-control">
                                    <label for="username">Username</label>
                                    <input type="text" name="username" class="form-control">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" class="form-control">
<!--                                    <label for="password">Password</label>-->
<!--                                    <input type="password" name="password" class="form-control">-->
                                </div>
                                <input type="submit" name="submit" class="myButton" value="Update">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include 'lib/footer.php';
?>