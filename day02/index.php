<?php
include 'header.php';
?>
    <section class="maincontent">
        <h2><u> Form Validation :</u></h2>
        <form method="post" action="<?php echo htmlspecialchars(s_SERVER['PHP_SELF']);?>">
            <div class="table-responsive">
                <table>
                    <tr>
                        <td></td>
                        <td><input type="text" name="name"class="contact" placeholder="Your name" required="required"/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="text" name="email" class="contact" placeholder="Email" required="required"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><textarea name="message" class="mcontact"  rows="5" cols="30" placeholder="Message" required="required"></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" class="myButton" value="SEND"/></td>
                        <td><input type="button" class="myButton" value=""></td>
                    </tr>
                </table>
            </div>
        </form>
        <?php
        $name = $email = $message ="";
        if($_SERVER["REQUEST_METHOD"]=="POST"){
            $name 		= validate(s_POST["name"]);
            $email 		= validate(s_POST["email"]);
            $message 	= validate(s_POST["message"]);
            echo $name ."<br/>";
            echo $email  ."<br/>";
            echo $message ."<br/>";
        }
        function validate($date){
            $date = trim($date);
            $date = stripcslashes($date);
            $date = htmlspecialchars($date);
            return $date;
        }

        ?>
    </section>
<?php
include 'footer.php';
?>